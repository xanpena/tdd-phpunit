<?php

use PHPUnit\Framework\TestCase;
use App\Slug;

class SlugTest extends TestCase
{
    public function test_render(){

        require "app/Slug.php"

        $slug = new Slug("Curso de TDD con PHPUnit");

        $expected = "cursos-de-tdd-con-phpunit";

        $this->assertEquals($slug->render(), $expected);
    }

    public function test_render_sin_espacios(){

        require "app/Slug.php"

        $slug = new Slug("        Curso de TDD con PHPUnit           ");

        $expected = "cursos-de-tdd-con-phpunit";

        $this->assertEquals($slug->render(), $expected);
    }
}