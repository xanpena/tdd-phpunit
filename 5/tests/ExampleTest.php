<?php

use PHPUnit\Framework\TestCase;

class ExampleTest extends TestCase
{
    public function testTrue()
    {
        $this->assertTrue(true);
        //$this->assertFalse(false);
    }

    public function testEquals()
    {
        $result = 5 + 5;

        $this-assertEquals($result, 10);
    }

    public function testEquals()
    {
        $result = 5 + 5;

        $this-assertSame($result, "10");
    }

    public function testArray()
    {
        $this->assertIsArray([]);
    }

    public function testCount()
    {
        $this->assertCount(2, ['Amarillo', 'Azul']);
    }

    public function testHasKey()
    {
        $this->assertArrayHasKey('color', ['color' => 'Azul']);
    }
}