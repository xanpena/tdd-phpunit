<?php

use PHPUnit\Framework\TestCase;

class SlugTest extends TestCase
{
    public function test_render(){

        require "app/Slug.php"

        $slug = new Slug("Curso de TDD con PHPUnit");

        $expected = "cursos-de-tdd-con-phpunit";

        $this->assertEquals($slug->render(), $expected);
    }
}