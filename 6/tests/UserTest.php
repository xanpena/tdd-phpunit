<?php

use PHPUnit\Framework\TestCase;
use App\User;

class UserTest extends TestCase
{
    public function setUp() : void
    {
        $this->user = new User;
    }
    
    public function test_i_can_the_name()
    {
        $this->user->setName('Xan');

        $this->assertEquals($this->user->getName(), "Xan");
    }

    public function test_i_can_the_lastname()
    {
        $this->user->setLastName("Pena");

        $this->assertEquals($this->user->getLastName(), "Pena");
    }

    public function test_i_can_the_email()
    {
        $this->user->setEmail("mail@example.com");
        
        $this->assertEquals($this->user->getEmail(), "mail@example.com")
    }

    public function test_get_the_fullname()
    {
        $this->user->setName('Xan');
        $this->user->setLastName("Pena");
        
        $this->assertEquals($this->user->getFullName(), "Xan Pena")
    }

    public function test_get_the_fullname_empty()
    {
        $this->assertEmpty($this->user->getFullName())
    }

    public function test_name_lastname_email_without_spaces()
    {
        $this->user->setName('          Xan');
        $this->user->setLastName('Xan       ');
        $this->user->setEmail('  mail@example.com   ');

        $this->assertEquals($this->user->getName(), "Xan")
        $this->assertEquals($this->user->getLastName(), "Pena")
        $this->assertEquals($this->user->getEmail(), "mail@example.com")
    }
}